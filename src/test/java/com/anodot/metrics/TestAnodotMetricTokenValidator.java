package com.anodot.metrics;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
@RunWith(JUnit4.class)
public class TestAnodotMetricTokenValidator {
    @Test
    public void test() {
        assertTrue(AnodotMetricTokenValidator.validate("aaa!"));
        assertFalse(AnodotMetricTokenValidator.validate("aa a"));
        assertFalse(AnodotMetricTokenValidator.validate("aa\ta"));
        assertFalse(AnodotMetricTokenValidator.validate("aaa="));
        assertFalse(AnodotMetricTokenValidator.validate("aaa='"));
        assertFalse(AnodotMetricTokenValidator.validate("aa.a'"));
    }
}