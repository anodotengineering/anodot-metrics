package com.anodot.metrics;

import com.anodot.metrics.metrics.ManagedHistogram;
import com.anodot.metrics.metrics.ManagedTimer;
import com.anodot.metrics.metrics.ManagedMetric;
import com.anodot.metrics.spec.MetricName;
import com.anodot.metrics.spec.Property;
import com.codahale.metrics.*;
import com.codahale.metrics.Timer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public class AnodotMetricRegistry implements AnodotMetricSet {
    private final ConcurrentMap<MetricName, Metric> metrics;
    private final ConcurrentMap<MetricName, Metric> managedMetrics;
    private final List<AnodotMetricRegistryListener> listeners;

    /**
     * Creates a new {@link MetricRegistry}.
     */
    public AnodotMetricRegistry() {
        this.metrics = buildMap();
        this.managedMetrics = new ConcurrentHashMap<>();
        this.listeners = new CopyOnWriteArrayList<AnodotMetricRegistryListener>();
    }

    /**
     * Creates a new {@link ConcurrentMap} implementation for use inside the registry. Override this
     * to create a {@link MetricRegistry} with space- or time-bounded metric lifecycles, for
     * example.
     *
     * @return a new {@link ConcurrentMap}
     */
    protected ConcurrentMap<MetricName, Metric> buildMap() {
        return new ConcurrentHashMap<MetricName, Metric>();
    }

    /**
     * Given a {@link Metric}, registers it under the given name.
     *
     * @param name   the name of the metric
     * @param metric the metric
     * @param <T>    the type of the metric
     * @return {@code metric}
     * @throws IllegalArgumentException if the name is already registered
     */
    @SuppressWarnings("unchecked")
    public <T extends Metric> T register(MetricName name, T metric) throws IllegalArgumentException {
        final Metric existing = metrics.putIfAbsent(name, metric);
        if (existing == null) {
            onMetricAdded(name, metric);
        } else {
            throw new IllegalArgumentException("A metric named " + name.asString() + " already exists");
        }

        return metric;
    }

    public <T extends Metric> T registerManagedMetric(MetricName name, T metric) throws IllegalArgumentException {
        final Metric existing = managedMetrics.putIfAbsent(name, metric);
        if (existing == null) {
            onMetricAdded(name, metric);
        } else {
            throw new IllegalArgumentException("A metric named " + name.asString() + " already exists");
        }

        return metric;
    }

    /**
     * Given a metric set, registers them.
     *
     * @param metrics a set of metrics
     * @throws IllegalArgumentException if any of the names are already registered
     */
    public void register(AnodotMetricSet metrics) throws IllegalArgumentException {
        for (Map.Entry<MetricName, Metric> entry : metrics.getMetrics().entrySet()) {
            register(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Creates a new {@link Counter} and registers it under the given name.
     *
     * @param name the name of the metric
     * @return a new {@link Counter}
     */
    public Counter counter(MetricName name) {
        return getOrAdd(name, MetricBuilder.COUNTERS);
    }

    /**
     * Creates a new {@link Histogram} and registers it under the given name.
     *
     * @param name the name of the metric
     * @return a new {@link Histogram}
     */
    public Histogram histogram(MetricName name) {
        return getOrAdd(name, MetricBuilder.HISTOGRAMS);
    }

    /**
     * Creates a new {@link Histogram} and registers it under the given name.
     *
     * @param name the name of the metric
     * @param percentiles the percentiles to be reported
     * @return a new {@link Histogram}
     */
    public Histogram histogram(MetricName name,
                               Set<Property> percentiles) {
        return getOrAdd(name, ManagedMetricBuilder.HISTOGRAMS, percentiles, null);
    }

    /**
     * Creates a new {@link Meter} and registers it under the given name.
     *
     * @param name the name of the metric
     * @return a new {@link Meter}
     */
    public Meter meter(MetricName name) {
        return getOrAdd(name, MetricBuilder.METERS);
    }

    /**
     * Creates a new {@link Timer} and registers it under the given name.
     *
     * @param name the name of the metric
     * @return a new {@link Timer}
     */
    public Timer timer(MetricName name) {
        return getOrAdd(name, MetricBuilder.TIMERS);
    }

    /**
     * Creates a new {@link Timer} and registers it under the given name.
     *
     * @param name the name of the metric
     * @param percentiles the percentiles to be reported
     * @param metered the meters to be reported
     * @return a new {@link Timer}
     */
    public Timer timer(MetricName name,
                       Set<Property> percentiles,
                       Set<Property> metered) {
        return getOrAdd(name, ManagedMetricBuilder.TIMERS, percentiles, metered);
    }

    /**
     * Removes the metric with the given name.
     *
     * @param name the name of the metric
     * @return whether or not the metric was removed
     */
    public boolean remove(MetricName name) {
        Metric metric = metrics.remove(name);
        if (metric == null) {
            metric = managedMetrics.remove(name);
        }
        final Metric removedMetric = metric;

        if (removedMetric != null) {
            onMetricRemoved(name, removedMetric);
            return true;
        }
        return false;
    }

    /**
     * Removes all metrics which match the given filter.
     *
     * @param filter a filter
     */
    public void removeMatching(AnodotMetricFilter filter) {
        for (Map.Entry<MetricName, Metric> entry : metrics.entrySet()) {
            if (filter.matches(entry.getKey(), entry.getValue())) {
                remove(entry.getKey());
            }
        }
    }

    /**
     * Adds a {@link MetricRegistryListener} to a collection of listeners that will be notified on
     * metric creation.  Listeners will be notified in the order in which they are added.
     * <p/>
     * <b>N.B.:</b> The listener will be notified of all existing metrics when it first registers.
     *
     * @param listener the listener that will be notified
     */
    public void addListener(AnodotMetricRegistryListener listener) {
        listeners.add(listener);

        for (Map.Entry<MetricName, Metric> entry : metrics.entrySet()) {
            notifyListenerOfAddedMetric(listener, entry.getValue(), entry.getKey());
        }

        for (Map.Entry<MetricName, Metric> entry : managedMetrics.entrySet()) {
            notifyListenerOfAddedMetric(listener, entry.getValue(), entry.getKey());
        }

    }

    /**
     * Removes a {@link MetricRegistryListener} from this registry's collection of listeners.
     *
     * @param listener the listener that will be removed
     */
    public void removeListener(AnodotMetricRegistryListener listener) {
        listeners.remove(listener);
    }

    /**
     * Returns a set of the names of all the metrics in the registry.
     *
     * @return the names of all the metrics
     */
    public SortedSet<MetricName> getNames() {
        return Collections.unmodifiableSortedSet(new TreeSet<MetricName>(metrics.keySet()));
    }

    /**
     * Returns a map of all the gauges in the registry and their names.
     *
     * @return all the gauges in the registry
     */
    public SortedMap<MetricName, Gauge> getGauges() {
        return getGauges(AnodotMetricFilter.ALL);
    }

    /**
     * Returns a map of all the gauges in the registry and their names which match the given filter.
     *
     * @param filter the metric filter to match
     * @return all the gauges in the registry
     */
    public SortedMap<MetricName, Gauge> getGauges(AnodotMetricFilter filter) {
        return getMetrics(Gauge.class, filter);
    }

    /**
     * Returns a map of all the counters in the registry and their names.
     *
     * @return all the counters in the registry
     */
    public SortedMap<MetricName, Counter> getCounters() {
        return getCounters(AnodotMetricFilter.ALL);
    }

    /**
     * Returns a map of all the counters in the registry and their names which match the given
     * filter.
     *
     * @param filter the metric filter to match
     * @return all the counters in the registry
     */
    public SortedMap<MetricName, Counter> getCounters(AnodotMetricFilter filter) {
        return getMetrics(Counter.class, filter);
    }

    public SortedMap<MetricName, ManagedMetric> getManagedTimers() {
        return getManagedMetrics(ManagedTimer.class, AnodotMetricFilter.ALL);
    }

    public SortedMap<MetricName, ManagedMetric> getManagedTimers(AnodotMetricFilter filter) {
        return getManagedMetrics(ManagedTimer.class, filter);
    }

    /**
     * Returns a map of all the histograms in the registry and their names.
     *
     * @return all the histograms in the registry
     */
    public SortedMap<MetricName, Histogram> getHistograms() {
        return getHistograms(AnodotMetricFilter.ALL);
    }

    public SortedMap<MetricName, ManagedMetric> getManagedHistograms() {
        return getManagedHistograms(AnodotMetricFilter.ALL);
    }

    public SortedMap<MetricName, ManagedMetric> getManagedHistograms(AnodotMetricFilter filter) {
        return getManagedMetrics(ManagedHistogram.class, filter);
    }

    /**
     * Returns a map of all the histograms in the registry and their names which match the given
     * filter.
     *
     * @param filter the metric filter to match
     * @return all the histograms in the registry
     */
    public SortedMap<MetricName, Histogram> getHistograms(AnodotMetricFilter filter) {
        return getMetrics(Histogram.class, filter);
    }

    /**
     * Returns a map of all the meters in the registry and their names.
     *
     * @return all the meters in the registry
     */
    public SortedMap<MetricName, Meter> getMeters() {
        return getMeters(AnodotMetricFilter.ALL);
    }

    /**
     * Returns a map of all the meters in the registry and their names which match the given filter.
     *
     * @param filter the metric filter to match
     * @return all the meters in the registry
     */
    public SortedMap<MetricName, Meter> getMeters(AnodotMetricFilter filter) {
        return getMetrics(Meter.class, filter);
    }

    /**
     * Returns a map of all the timers in the registry and their names.
     *
     * @return all the timers in the registry
     */
    public SortedMap<MetricName, Timer> getTimers() {
        return getTimers(AnodotMetricFilter.ALL);
    }

    /**
     * Returns a map of all the timers in the registry and their names which match the given filter.
     *
     * @param filter the metric filter to match
     * @return all the timers in the registry
     */
    public SortedMap<MetricName, Timer> getTimers(AnodotMetricFilter filter) {
        return getMetrics(Timer.class, filter);
    }

    @SuppressWarnings("unchecked")
    private <T extends Metric> T getOrAdd(MetricName name, MetricBuilder<T> builder) {
        final Metric metric = metrics.get(name);
        if (builder.isInstance(metric)) {
            return (T) metric;
        } else if (metric == null) {
            try {
                return register(name, builder.newMetric());
            } catch (IllegalArgumentException e) {
                final Metric added = metrics.get(name);
                if (builder.isInstance(added)) {
                    return (T) added;
                }
            }
        }
        throw new IllegalArgumentException(name + " is already used for a different type of metric");
    }

    private <T extends Metric> T getOrAdd(MetricName name,
                                          ManagedMetricBuilder<T> builder,
                                          Set<Property> percentiles,
                                          Set<Property> metered) {
        final Metric metric = managedMetrics.get(name);
        if (builder.isInstance(metric)) {
            return (T) metric;
        } else if (metric == null) {
            try {
                return registerManagedMetric(name, builder.newMetric(percentiles, metered));
            } catch (IllegalArgumentException e) {
                final Metric added = managedMetrics.get(name);
                if (builder.isInstance(added)) {
                    return (T) added;
                }
            }
        }
        throw new IllegalArgumentException(name + " is already used for a different type of metric");
    }


    @SuppressWarnings("unchecked")
    private <T extends Metric> SortedMap<MetricName, T> getMetrics(Class<T> klass, AnodotMetricFilter filter) {
        final TreeMap<MetricName, T> timers = new TreeMap<MetricName, T>();
        for (Map.Entry<MetricName, Metric> entry : metrics.entrySet()) {
            if (klass.isInstance(entry.getValue()) && filter.matches(entry.getKey(),
                    entry.getValue())) {
                timers.put(entry.getKey(), (T) entry.getValue());
            }
        }
        return Collections.unmodifiableSortedMap(timers);
    }

    private <T extends ManagedMetric> SortedMap<MetricName, ManagedMetric> getManagedMetrics(Class<T> klass, AnodotMetricFilter filter) {
        final TreeMap<MetricName, T> filteredMetrics = new TreeMap<MetricName, T>();
        for (Map.Entry<MetricName, Metric> entry : managedMetrics.entrySet()) {
            if (klass.isInstance(entry.getValue()) && filter.matches(entry.getKey(),
                    entry.getValue())) {
                filteredMetrics.put(entry.getKey(), (T) entry.getValue());
            }
        }
        return Collections.unmodifiableSortedMap(filteredMetrics);
    }

    private void onMetricAdded(MetricName name, Metric metric) {
        for (AnodotMetricRegistryListener listener : listeners) {
            notifyListenerOfAddedMetric(listener, metric, name);
        }
    }

    private void notifyListenerOfAddedMetric(AnodotMetricRegistryListener listener, Metric metric, MetricName name) {
        if (metric instanceof Gauge) {
            listener.onGaugeAdded(name, (Gauge<?>) metric);
        } else if (metric instanceof Counter) {
            listener.onCounterAdded(name, (Counter) metric);
        } else if (metric instanceof Histogram) {
            listener.onHistogramAdded(name, (Histogram) metric);
        } else if (metric instanceof Meter) {
            listener.onMeterAdded(name, (Meter) metric);
        } else if (metric instanceof Timer) {
            listener.onTimerAdded(name, (Timer) metric);
        } else {
            throw new IllegalArgumentException("Unknown metric type: " + metric.getClass());
        }
    }

    private void onMetricRemoved(MetricName name, Metric metric) {
        for (AnodotMetricRegistryListener listener : listeners) {
            notifyListenerOfRemovedMetric(name, metric, listener);
        }
    }

    private void notifyListenerOfRemovedMetric(MetricName name, Metric metric, AnodotMetricRegistryListener listener) {
        if (metric instanceof Gauge) {
            listener.onGaugeRemoved(name);
        } else if (metric instanceof Counter) {
            listener.onCounterRemoved(name);
        } else if (metric instanceof Histogram) {
            listener.onHistogramRemoved(name);
        } else if (metric instanceof Meter) {
            listener.onMeterRemoved(name);
        } else if (metric instanceof Timer) {
            listener.onTimerRemoved(name);
        } else {
            throw new IllegalArgumentException("Unknown metric type: " + metric.getClass());
        }
    }

    public Map<MetricName, Metric> getMetrics() {
        return Collections.unmodifiableMap(metrics);
    }

    /**
     * A quick and easy way of capturing the notion of default metrics.
     */
    private interface MetricBuilder<T extends Metric> {
        MetricBuilder<Counter> COUNTERS = new MetricBuilder<Counter>() {
            public Counter newMetric() {
                return new Counter();
            }

            public boolean isInstance(Metric metric) {
                return Counter.class.isInstance(metric);
            }
        };

        MetricBuilder<Histogram> HISTOGRAMS = new MetricBuilder<Histogram>() {
            public Histogram newMetric() {
                return new Histogram(new ExponentiallyDecayingReservoir());
            }

            public boolean isInstance(Metric metric) {
                return Histogram.class.isInstance(metric);
            }
        };

        MetricBuilder<Meter> METERS = new MetricBuilder<Meter>() {
            public Meter newMetric() {
                return new Meter();
            }

            public boolean isInstance(Metric metric) {
                return Meter.class.isInstance(metric);
            }
        };

        MetricBuilder<Timer> TIMERS = new MetricBuilder<Timer>() {
            public Timer newMetric() {
                return new Timer();
            }

            public boolean isInstance(Metric metric) {
                return Timer.class.isInstance(metric);
            }
        };

        T newMetric();

        boolean isInstance(Metric metric);
    }

    private interface ManagedMetricBuilder<T extends Metric> {

        ManagedMetricBuilder<ManagedHistogram> HISTOGRAMS = new ManagedMetricBuilder<ManagedHistogram>() {
            public ManagedHistogram newMetric(Set<Property> percentiles, Set<Property> metered) {
                return new ManagedHistogram(new ExponentiallyDecayingReservoir(), percentiles);
            }

            public boolean isInstance(Metric metric) {
                return Histogram.class.isInstance(metric);
            }
        };

        ManagedMetricBuilder<ManagedTimer> TIMERS = new ManagedMetricBuilder<ManagedTimer>() {
            public ManagedTimer newMetric(Set<Property> percentiles, Set<Property> metered) {
                return new ManagedTimer(percentiles, metered);
            }

            public boolean isInstance(Metric metric) {
                return ManagedTimer.class.isInstance(metric);
            }
        };

        T newMetric(Set<Property> percentiles,
                    Set<Property> metered);

        boolean isInstance(Metric metric);
    }
}