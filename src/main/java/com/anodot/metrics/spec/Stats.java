package com.anodot.metrics.spec;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public enum Stats {
    LOWER("min"),                 // lowest value seen
    UPPER("max"),                 // highest value seen
    MEAN("mean"),                   // standard mean
    STANDARD_DEVIATION("std"),      // standard deviation
    P50("p50"),                     // the 50th percentile
    P75("p75"),                     // the 75th percentile
    P95("p95"),                     // the 95th percentile
    P98("p98"),                     // the 98th percentile
    P99("p99"),                     // the 99th percentile
    P999("p999");                   // the 999th percentile

    private final String name;

    Stats(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
