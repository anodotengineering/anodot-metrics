package com.anodot.metrics.spec;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 *
 * The unit tag is one of the most import tags. It represents the unit of magnitude of the measured quantity. It's important that every metric has the correct value for the unit tag.
 * Units in metrics 2.0 are SI units (base and derived) and IEC units, extended with units commonly used in IT (the "extensions").
 * The extensions are designed to be intuitive (i.e. as commonly used by strftime), however, are to never conflict with SI or IEC.
 */
public enum Unit {
    UNKNOWN("unknown"),                             // for legacy inputs where you don't know the unit. (structured metrics catchall plugins use this)
    BIT("b"),                                       // bits
    BYTE("B"),                                      // byte
    SECOND("S"),                                    // second (strftime)
    MINUTE("M"),                                    // minute (strftime)
    HOUR("h"),                                      // hour (strftime)
    DAY("d"),                                       // day (strftime)
    WEEK("w"),                                      // week (strftime)
    MONTH("mo"),                                    // month (strftime)
    ERRORS("Err"),                                  // errors
    WARN("Warn"),                                   // warnings
    CONNECTIONS("Conn"),                            // connections
    EVENTS("Event"),                                // events (TCP events etc)
    INODES("Ino"),                                  // inodes
    EMAILS("Email"),                                // email messages
    JIFFIES("Jiff"),                                // jiffies (i.e. for cpu usage)
    JOB("Job"),                                     // job (as in job queue)
    FILE("File"),                                   // (not 'F' that's farad)
    LOAD("Load"),                                   // cpu load
    METRIC("Metric"),                               // a metric line like in the statsd or graphite protocol
    MESSAGE("Msg"),                                 // message (like in message queues)
    PROBABILITY("P"),                               // probability (between 0 and 1)
    PAGE("Page"),                                   // page (as in memory segment)
    PACKET("Pckt"),                                 // network packet
    PROCESS("Process"),                             // process
    REQUEST("Req"),                                 // http requests, database queries, etc
    SOCK("Sock"),                                   // sockets
    THREAD("Thread"),                               // thread
    TICKET("Ticket"),                               // upload tickets, kerberos tickets, ..
    NANO("ns"),                                     // nano, 10^-9
    MICRO(String.valueOf('\u00B5')),                // micro, 10^-6
    MILLI("m"),                                     // milli, 10^-3
    CENTI("c"),                                     // centi, 10^-2
    DECI("d"),                                      // deci, 10^-1
    KILO("k"),                                      // kilo, 10^3
    MEGA("M"),                                      // mega, 10^6
    GIGA("G"),                                      // giga, 10^9
    TERA("T"),                                      // tera, 10^12
    KIBI("Ki"),                                     // kibi, 1024
    MEBI("Mi"),                                     // mebi, 1024^2
    GIBI("Gi"),                                     // gibi, 1024^3
    TEBI("Ti"),                                     // tebi, 1024^4
    PEBI("Pi"),                                     // pebi, 1024^5
    EXBI("Ei");                                     // exbi, 1024^6

    private final String symbol;

    Unit(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
