package com.anodot.metrics.spec;

import java.util.*;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public class MetricNameWrapper {
    private final MetricName name;
    private final Set<Property> maskProperties;

    public MetricNameWrapper(MetricName name, Property... maskProperties) {
        this.name = name;
        this.maskProperties = new HashSet<Property>();
        for (Property property : maskProperties) {
            if (property != null) {
                this.maskProperties.add(property);
            }
        }
    }

    public String asString() {
        LinkedHashMap<String, Property> constructedName = new LinkedHashMap<String, Property>();
        for (Property property : name) {
            constructedName.put(property.getKey(), property);
        }

        for (Property property : maskProperties) {
            constructedName.put(property.getKey(), property);
        }

        return MetricName.concatenateProperties(constructedName.values());
    }

    @Override
    public String toString() {
        return asString();
    }
}
