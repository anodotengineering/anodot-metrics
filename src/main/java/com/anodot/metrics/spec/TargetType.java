package com.anodot.metrics.spec;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public enum TargetType {
    RATE("rate"),               // a number per some time unit
    GAUGE("gauge"),             // gauge	values at each point in time
    COUNTER("counter");         // counter	a number that keeps increasing over time (but might wrap/reset at some points) (no statsd counterpart), i.e. a gauge with the added notion of "i usually want to derive this"

    private final String name;

    TargetType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
