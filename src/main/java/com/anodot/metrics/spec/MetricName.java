package com.anodot.metrics.spec;


import com.anodot.metrics.AnodotMetricTokenValidator;

import java.util.*;

/**
 * User: Yonatan Ben-Simhon
 * Metric name implementing metrics 2.0 spec.
 *
 * @see <a href="http://metrics20.org/spec/">Metrics 2.0</a>
 */
public class MetricName implements Iterable<Property>, Comparable<MetricName> {
    public static final int MAX_PROPERTIES_LENGTH = 1000;
    public static final int MAX_TAGS_LENGTH = 2000;
    private final Map<String, Property> properties;
    private final ArrayList<Property> propertiesList;
    private final Map<String, Property> tags;
    private int propsLength;
    private int tagsLength;

    private String cachedName = null;

    private MetricName() {
        this.properties = new HashMap<String, Property>();
        this.tags = new HashMap<String, Property>();
        this.propertiesList = new ArrayList<Property>();
        this.propsLength = 0;
        this.tagsLength = 0;
    }

    private void addProperty(String key, Property property) {
        if (propsLength + key.length() + property.getValue().length() + 1 > MAX_PROPERTIES_LENGTH) {
            throw new IllegalArgumentException("metric properties reached maximum length: " + MAX_PROPERTIES_LENGTH);
        }
        if (properties.containsKey(key)) {
            throw new IllegalArgumentException("given property is already set");
        }

        properties.put(key, property);
        propertiesList.add(property);
        this.propsLength += key.length() + property.getValue().length() + 1;
    }

    private void addTag(String key, Property property) {
        if (tagsLength + key.length() + property.getValue().length() + 1 > MAX_TAGS_LENGTH) {
            throw new IllegalArgumentException("metric tags reached maximum length: " + MAX_TAGS_LENGTH);
        }
        if (tags.containsKey(key)) {
            throw new IllegalArgumentException("given tag is already set");
        }

        tags.put(key, property);
        this.tagsLength += key.length() + property.getValue().length() + 1;
    }

    public String asString() {
        if (cachedName == null) {
            cachedName = concatenateProperties(propertiesList);
        }

        return cachedName;
    }

    public static String concatenateProperties(Collection<Property> properties) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (Property property : properties) {
            sb.append(property.getKey());
            sb.append("=");
            sb.append(property.getValue());
            if (i < properties.size() - 1) {
                sb.append(".");
            }
            i++;
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return asString();
    }

    @Override
    public boolean equals(Object o) {
        return asString().equals(o.toString());
    }

    @Override
    public int hashCode() {
        return asString().hashCode();
    }

    @Override
    public int compareTo(MetricName o) {
        return asString().compareTo(o.asString());
    }

    private boolean isEmpty() {
        return this.properties.isEmpty();
    }

    public Iterator<Property> iterator() {
        return propertiesList.iterator();
    }

    /**
     * @param what the thing being measured.
     *             should not include the units or metric statistical terms,
     *             statistical terms (like mean, standard deviation, p50 etc..) are automatically added to the name.
     *             for units see {@link com.anodot.metrics.spec.MetricName.MetricNameBuilder#withUnit(Unit)}
     * @return name builder
     */
    public static MetricNameBuilder builder(String what) {
        return new MetricNameBuilder(what);
    }

    public Property getProperty(String key) {
        return properties.get(key);
    }

    public static final class MetricNameBuilder {

        private final MetricName name;

        public MetricNameBuilder(String what) {
            this.name = new MetricName();

            this.withPropertyValue(Properties.WHAT, what);
        }

        private void validateValue(String property, String value) {
            if (value == null) {
                throw new IllegalArgumentException("given value for property: " + property + " is null");
            }

            if (value.isEmpty()) {
                throw new IllegalArgumentException("given value for property: " + property + " is empty");
            }

            if (!AnodotMetricTokenValidator.validate(property)) {
                throw new IllegalArgumentException("given property is contains illegal characters. unsupported characters are: = ' and any white space character");
            }
        }

        private void validateProperty(String property) {
            if (property == null) {
                throw new IllegalArgumentException("given property is null");
            }

            if (property.isEmpty()) {
                throw new IllegalArgumentException("given property is empty");
            }

            if (!AnodotMetricTokenValidator.validate(property)) {
                throw new IllegalArgumentException("given property is contains illegal characters. unsupported characters are: = ' and any white space character");
            }
        }

        /**
         * add a custom property.
         * properties are what define the metric uniqueness. i.e. Anodot considers two metrics as identical iff their properties set are equal.
         *
         * @param property property name
         * @param value    property value
         * @return this for chained calls
         * @throws IllegalArgumentException for null or empty property names and values
         */
        public MetricNameBuilder withPropertyValue(String property, String value) {
            validateProperty(property);
            validateValue(property, value);
            this.name.addProperty(property, new Property(property, value));
            return this;
        }

        /**
         * add a custom tag.
         * tags are treated as hint for the metric and do not affect the metric uniqueness.
         *
         * @param tag   tag name
         * @param value tag value
         * @return this for chained calls
         * @throws IllegalArgumentException for null or empty tag names and values
         */
        public MetricNameBuilder withTagValue(String tag, String value) {
            validateProperty(tag);
            validateValue(tag, value);
            this.name.addTag(tag, new Property(tag, value));
            return this;
        }

        /**
         * @param server physical or virtual machine
         * @return this for chained calls
         */
        public MetricNameBuilder withServer(String server) {
            withPropertyValue(Properties.SERVER, server);
            return this;
        }

        /**
         * @param httpMethod the http method. like PUT, GET, etc
         * @return this for chained calls
         */
        public MetricNameBuilder withHttpMethod(String httpMethod) {
            withPropertyValue(Properties.HTTP_METHOD, httpMethod);
            return this;
        }

        /**
         * @param httpCode 200, 404, etc
         * @return this for chained calls
         */
        public MetricNameBuilder withHttpCode(String httpCode) {
            withPropertyValue(Properties.HTTP_CODE, httpCode);
            return this;
        }

        /**
         * @param device block device, network device...
         * @return this for chained calls
         */
        public MetricNameBuilder withDevice(String device) {
            withPropertyValue(Properties.DEVICE, device);
            return this;
        }

        /**
         * @param type further describe the metric. type is a very generic word, only use it if you really don't know anything better.
         * @return this for chained calls
         */
        public MetricNameBuilder withType(String type) {
            withPropertyValue(Properties.TYPE, type);
            return this;
        }

        /**
         * @param result values: ok, fail ... (for http requests, http_code is probably more useful)
         * @return this for chained calls
         */
        public MetricNameBuilder withResult(String result) {
            withPropertyValue(Properties.RESULT, result);
            return this;
        }

        /**
         * @param unit the unit something is expressed in (b/s, MB, etc)
         * @return this for chained calls
         */
        public MetricNameBuilder withUnit(String unit) {
            withPropertyValue(Properties.UNIT, unit);
            return this;
        }

        /**
         * @param unit the unit
         * @return this for chained calls
         */
        public MetricNameBuilder withUnit(Unit unit) {
            withPropertyValue(Properties.UNIT, unit.getSymbol());
            return this;
        }

        public MetricName build() {
            if (name.isEmpty()) {
                throw new IllegalArgumentException("no properties are defined");
            }
            return name;
        }

    }
}
