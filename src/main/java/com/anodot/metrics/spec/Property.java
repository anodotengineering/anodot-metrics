package com.anodot.metrics.spec;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public class Property implements Comparable<Property> {
    private final String key;
    private final String value;

    public Property(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Property)) return false;

        Property property = (Property) o;

        if (!key.equals(property.key)) return false;
        return value.equals(property.value);

    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }

    @Override
    public int compareTo(Property o) {
        return this.key.compareTo(o.key);
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return key + "=" + value;
    }
}
