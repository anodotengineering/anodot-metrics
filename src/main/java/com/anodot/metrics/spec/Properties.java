package com.anodot.metrics.spec;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public class Properties {
    public static final String SERVER = "server";    // physical or virtual machine
    public static final String HTTP_METHOD = "http_method";    // the http method. like PUT, GET, etc
    public static final String HTTP_CODE = "http_code";    // 200, 404, etc
    public static final String DEVICE = "device";    // block device, network device...
    public static final String UNIT	= "unit"; // the unit something is expressed in (b/s, MB, etc)
    public static final String WHAT = "what"; // the thing being measured
    public static final String TYPE = "type"; // further describe the metric. type is a very generic word, only use it if you really don't know anything better.
    public static final String RESULT = "result"; // values: ok, fail ... (for http requests, http_code is probably more useful)
    public static final String STAT = "stat"; // if the metric is a statistical view or summary statistic. see Stats for possible values
    public static final String INTERVAL = "interval"; // time period measurement is based on.
    public static final String TARGET_TYPE = "target_type";   // aka metric type. see TargetType for possible values
}
