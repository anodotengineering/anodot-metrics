package com.anodot.metrics;

import com.anodot.metrics.spec.MetricName;
import com.codahale.metrics.Metric;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public interface AnodotMetricFilter {
    /**
     * Matches all metrics, regardless of type or name.
     */
    AnodotMetricFilter ALL = new AnodotMetricFilter() {
        public boolean matches(MetricName name, Metric metric) {
            return true;
        }
    };

    /**
     * Returns {@code true} if the metric matches the filter; {@code false} otherwise.
     *
     * @param name   the metric's name
     * @param metric the metric
     * @return {@code true} if the metric matches the filter
     */
    boolean matches(MetricName name, Metric metric);
}
