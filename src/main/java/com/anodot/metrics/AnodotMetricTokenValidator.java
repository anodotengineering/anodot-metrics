package com.anodot.metrics;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public class AnodotMetricTokenValidator {
    private final static Pattern TOKEN_INVALID_PATTERN = Pattern.compile("[\\s'=\\.]+");

    public static boolean validate(String token) {
        Matcher matcher = TOKEN_INVALID_PATTERN.matcher(token);
        return !matcher.find();
    }
}
