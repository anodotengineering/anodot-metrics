package com.anodot.metrics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * sends data using Anodot API
 *
 * @author josephm
 */
public class Anodot {
    private static final Logger LOGGER = LoggerFactory.getLogger(Anodot.class);
    private List<Metric> metrics = new LinkedList<Metric>();

    private final String uri;
    private final String token;
    private HttpSender sender;

    private long failures;

    public Anodot(String token) {
        this("https://api.anodot.com/api/v1/metrics", token);
    }

    public Anodot(String baseUri, String token) {
        this(baseUri, token, new HttpSender());
    }

    public Anodot(String uri, String token, HttpSender httpsender) {
        this.uri = uri;
        if (token == null || token.isEmpty()) {
            throw new IllegalArgumentException("given token is empty or null");
        }
        this.token = token;
        this.sender = httpsender;
    }

    public void send(String name, String value, long timestamp, String targetType) throws IOException {
        metrics.add(new Metric(name, timestamp, value, targetType));
    }

    public void flush() throws IOException {
        try {
            sendAll();
        } finally {
            metrics.clear();
        }
    }

    public void close() throws IOException {
        try {
            flush();
        } catch (IOException ex) {
            LOGGER.error("failed during flush", ex);
        }
    }

    public long getFailures() {
        return failures;
    }

    private void sendAll() throws IOException {
        if (!metrics.isEmpty()) {
            try {
                String json = toJson(metrics);

                URI uri = new URI(this.uri + "?token=" + token);

                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info("sending: {} samples to Anodot. total failures: {}", metrics.size(), failures);
                }
                this.sender.post(uri, json);

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(json);
                }


            } catch (IOException e) {
                this.failures++;
                throw e;
            } catch (URISyntaxException e1) {
                this.failures++;
            }
        }
    }

    private String toJson(List<Metric> metrics) throws IOException {
        StringBuilder buf = new StringBuilder(1000);
        // [{"name":"<Metric Name>","timestamp":<epoc time>,"value":<value>,"tags":{"target_type":"counter"}}]
        // array start
        buf.append("[");

        for (Iterator<Metric> i = metrics.iterator(); i.hasNext(); ) {
            Metric m = i.next();

            // item start
            buf.append("{");

            buf.append("\"name\":").append("\"").append(m.name).append("\",");
            buf.append("\"timestamp\":").append(m.timestamp).append(",");
            buf.append("\"value\":").append(m.value).append("").append(",");
            buf.append("\"tags\":");
            // tags start
            buf.append("{\"");
            buf.append("target_type\":\"").append(m.targetType);
            // tags end
            buf.append("\"}");

            // item end
            buf.append("}");

            if (i.hasNext()) {
                buf.append(",");
            }
        }

        // array end
        buf.append("]");

        return buf.toString();
    }

    static class Metric {
        private String name;
        private long timestamp;
        private String value;
        private String targetType;

        Metric(String name, long timestamp, String value, String targetType) {
            this.name = name;
            this.timestamp = timestamp;
            this.value = value;
            this.targetType = targetType;
        }
    }

}
