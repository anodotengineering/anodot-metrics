package com.anodot.metrics;

import com.anodot.metrics.spec.MetricName;
import com.codahale.metrics.*;

import java.util.EventListener;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public interface AnodotMetricRegistryListener extends EventListener {
    /**
     * A no-op implementation of {@link AnodotMetricRegistryListener}.
     */
    abstract class Base implements AnodotMetricRegistryListener {
        public void onGaugeAdded(MetricName name, Gauge<?> gauge) {
        }

        public void onGaugeRemoved(MetricName name) {
        }

        public void onCounterAdded(MetricName name, Counter counter) {
        }

        public void onCounterRemoved(MetricName name) {
        }

        public void onHistogramAdded(MetricName name, Histogram histogram) {
        }

        public void onHistogramRemoved(MetricName name) {
        }

        public void onMeterAdded(MetricName name, Meter meter) {
        }

        public void onMeterRemoved(MetricName name) {
        }

        public void onTimerAdded(MetricName name, Timer timer) {
        }

        public void onTimerRemoved(MetricName name) {
        }
    }

    /**
     * Called when a {@link Gauge} is added to the registry.
     *
     * @param name  the gauge's name
     * @param gauge the gauge
     */
    void onGaugeAdded(MetricName name, Gauge<?> gauge);

    /**
     * Called when a {@link Gauge} is removed from the registry.
     *
     * @param name the gauge's name
     */
    void onGaugeRemoved(MetricName name);

    /**
     * Called when a {@link Counter} is added to the registry.
     *
     * @param name    the counter's name
     * @param counter the counter
     */
    void onCounterAdded(MetricName name, Counter counter);

    /**
     * Called when a {@link Counter} is removed from the registry.
     *
     * @param name the counter's name
     */
    void onCounterRemoved(MetricName name);

    /**
     * Called when a {@link Histogram} is added to the registry.
     *
     * @param name      the histogram's name
     * @param histogram the histogram
     */
    void onHistogramAdded(MetricName name, Histogram histogram);

    /**
     * Called when a {@link Histogram} is removed from the registry.
     *
     * @param name the histogram's name
     */
    void onHistogramRemoved(MetricName name);

    /**
     * Called when a {@link Meter} is added to the registry.
     *
     * @param name  the meter's name
     * @param meter the meter
     */
    void onMeterAdded(MetricName name, Meter meter);

    /**
     * Called when a {@link Meter} is removed from the registry.
     *
     * @param name the meter's name
     */
    void onMeterRemoved(MetricName name);

    /**
     * Called when a {@link Timer} is added to the registry.
     *
     * @param name  the timer's name
     * @param timer the timer
     */
    void onTimerAdded(MetricName name, Timer timer);

    /**
     * Called when a {@link Timer} is removed from the registry.
     *
     * @param name the timer's name
     */
    void onTimerRemoved(MetricName name);
}