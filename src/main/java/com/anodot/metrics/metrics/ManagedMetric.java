package com.anodot.metrics.metrics;

import java.util.List;

public interface ManagedMetric {

    List<ReportInfo> getPercentiles();

    List<ReportInfo> getMetered();
}
