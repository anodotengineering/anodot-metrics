package com.anodot.metrics.metrics;

import com.anodot.metrics.AnodotReporter;
import com.anodot.metrics.spec.Property;
import com.anodot.metrics.spec.TargetType;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Metric;
import com.codahale.metrics.Reservoir;

import java.util.*;
import java.util.stream.Collectors;

public class ManagedHistogram extends Histogram implements Metric, ManagedMetric {

    private final Set<Property> percentiles;

    private static Map<Property, ManagedHistogram.ValueExtractor> percentilesValueGetter = new HashMap<Property, ManagedHistogram.ValueExtractor>()
    {{
        put(AnodotReporter.STATS_UPPER, (x) -> x.getSnapshot().getMax());
        put(AnodotReporter.STATS_MEAN, (x) -> x.getSnapshot().getMean());
        put(AnodotReporter.STATS_LOWER, (x) -> x.getSnapshot().getMin());
        put(AnodotReporter.STATS_STD, (x) -> x.getSnapshot().getStdDev());
        put(AnodotReporter.STATS_P50, (x) -> x.getSnapshot().getMedian());
        put(AnodotReporter.STATS_P75, (x) -> x.getSnapshot().get75thPercentile());
        put(AnodotReporter.STATS_P95, (x) -> x.getSnapshot().get95thPercentile());
        put(AnodotReporter.STATS_P98, (x) -> x.getSnapshot().get98thPercentile());
        put(AnodotReporter.STATS_P99, (x) -> x.getSnapshot().get99thPercentile());
        put(AnodotReporter.STATS_P999, (x) -> x.getSnapshot().get999thPercentile());
    }};

    public ManagedHistogram(Reservoir reservoir,
                            Set<Property> percentiles) {
        super(reservoir);
        this.percentiles = percentiles != null?
                percentiles.stream().filter(x -> percentilesValueGetter.containsKey(x)).collect(Collectors.toSet()) :
                new HashSet<>();

    }

    @Override
    public List<ReportInfo> getPercentiles() {
        return percentiles.stream()
                .map(percentile ->
                        new ReportInfo<>(percentile,
                                percentilesValueGetter.get(percentile).get(this),
                                TargetType.GAUGE.getName())).collect(Collectors.toList());
    }

    @Override
    public List<ReportInfo> getMetered() {
        return new ArrayList<>();
    }

    private interface ValueExtractor<T extends Number> {
        T get(Histogram histogram);
    }
}
