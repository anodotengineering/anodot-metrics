package com.anodot.metrics.metrics;

import com.anodot.metrics.spec.Property;

public class ReportInfo<T extends Number> {

    private Property property;
    private T value;
    private String name;

    ReportInfo(Property property, T value, String name) {
        this.property = property;
        this.value = value;
        this.name = name;
    }

    public Property getProperty() {
        return property;
    }

    public T getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

}
