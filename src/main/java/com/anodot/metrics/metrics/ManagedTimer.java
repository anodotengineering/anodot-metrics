package com.anodot.metrics.metrics;

import com.anodot.metrics.AnodotReporter;
import com.anodot.metrics.spec.*;
import com.codahale.metrics.Metric;
import com.codahale.metrics.Timer;

import java.util.*;
import java.util.stream.Collectors;

public class ManagedTimer extends Timer implements Metric, ManagedMetric {

    private final Set<Property> percentiles;
    private final Set<Property> metered;

    private static Map<Property, ValueExtractor> percentilesValueGetter = new HashMap<Property, ValueExtractor>()
    {{
        put(AnodotReporter.STATS_UPPER, (x) -> x.getSnapshot().getMax());
        put(AnodotReporter.STATS_MEAN, (x) -> x.getSnapshot().getMean());
        put(AnodotReporter.STATS_LOWER, (x) -> x.getSnapshot().getMin());
        put(AnodotReporter.STATS_STD, (x) -> x.getSnapshot().getStdDev());
        put(AnodotReporter.STATS_P50, (x) -> x.getSnapshot().getMedian());
        put(AnodotReporter.STATS_P75, (x) -> x.getSnapshot().get75thPercentile());
        put(AnodotReporter.STATS_P95, (x) -> x.getSnapshot().get95thPercentile());
        put(AnodotReporter.STATS_P98, (x) -> x.getSnapshot().get98thPercentile());
        put(AnodotReporter.STATS_P99, (x) -> x.getSnapshot().get99thPercentile());
        put(AnodotReporter.STATS_P999, (x) -> x.getSnapshot().get999thPercentile());
    }};

    private static Map<Property, ValueExtractor> meteredValueGetter = new HashMap<Property, ValueExtractor>() {{
        put(AnodotReporter.INTERVAL_M1, Timer::getOneMinuteRate);
        put(AnodotReporter.INTERVAL_M5, Timer::getFiveMinuteRate);
        put(AnodotReporter.INTERVAL_M15, Timer::getFifteenMinuteRate);
        put(AnodotReporter.STATS_MEAN, Timer::getMeanRate);
    }};

    public ManagedTimer(Set<Property> percentiles) {
        this(percentiles, null);
    }

    public ManagedTimer(Set<Property> percentiles,
                        Set<Property> metered) {
        this.percentiles = percentiles != null?
                percentiles.stream().filter(x -> percentilesValueGetter.containsKey(x)).collect(Collectors.toSet()) :
                new HashSet<>();
        this.metered = metered != null?
                metered.stream().filter(x -> meteredValueGetter.containsKey(x)).collect(Collectors.toSet()) :
                new HashSet<>();
    }

    @Override
    public List<ReportInfo> getPercentiles() {
        return percentiles.stream()
                .map(percentile ->
                        new ReportInfo<>(percentile,
                                percentilesValueGetter.get(percentile).get(this),
                                TargetType.GAUGE.getName())).collect(Collectors.toList());
    }

    @Override
    public List<ReportInfo> getMetered() {
        return metered.stream()
                .map(meter ->
                        new ReportInfo<>(
                                meter,
                                meteredValueGetter.get(meter).get(this),
                                TargetType.GAUGE.getName())).collect(Collectors.toList());

    }


    private interface ValueExtractor<T extends Number> {
        T get(Timer timer);
    }
}
