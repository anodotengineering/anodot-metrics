package com.anodot.metrics;

import com.anodot.metrics.metrics.ReportInfo;
import com.anodot.metrics.metrics.ManagedMetric;
import com.anodot.metrics.spec.*;
import com.codahale.metrics.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: Yonatan Ben-Simhon
 * A reporter which publishes metric values to a Anodot server.
 *
 * @see <a href="http://http://www.anodot.com//">Anodot - Scalable Realtime Anomaly Detection and Correlation</a>
 */
public class AnodotReporter implements Closeable {
    private static final Logger LOGGER = LoggerFactory.getLogger(AnodotReporter.class);
    public static final Property GAUGE_TARGET_TYPE = new Property(Properties.TARGET_TYPE, TargetType.GAUGE.getName());
    public static final Property RATE_TARGET_TYPE = new Property(Properties.TARGET_TYPE, TargetType.RATE.getName());
    public static final Property COUNTER_TARGET_TYPE = new Property(Properties.TARGET_TYPE, TargetType.COUNTER.getName());

    public static final Property INTERVAL_M1 = new Property(Properties.INTERVAL, "last_1m");
    public static final Property INTERVAL_M5 = new Property(Properties.INTERVAL, "last_5m");
    public static final Property INTERVAL_M15 = new Property(Properties.INTERVAL, "last_15m");

    public static final Property STATS_UPPER = new Property(Properties.STAT, Stats.UPPER.getName());
    public static final Property STATS_MEAN = new Property(Properties.STAT, Stats.MEAN.getName());
    public static final Property STATS_LOWER = new Property(Properties.STAT, Stats.LOWER.getName());
    public static final Property STATS_STD = new Property(Properties.STAT, Stats.STANDARD_DEVIATION.getName());
    public static final Property STATS_P50 = new Property(Properties.STAT, Stats.P50.getName());
    public static final Property STATS_P75 = new Property(Properties.STAT, Stats.P75.getName());
    public static final Property STATS_P95 = new Property(Properties.STAT, Stats.P95.getName());
    public static final Property STATS_P98 = new Property(Properties.STAT, Stats.P98.getName());
    public static final Property STATS_P99 = new Property(Properties.STAT, Stats.P99.getName());
    public static final Property STATS_P999 = new Property(Properties.STAT, Stats.P999.getName());

    private final AnodotMetricRegistry registry;
    private final ScheduledExecutorService executor;
    private final AnodotMetricFilter filter;

    private final TimeUnit durationUnit;
    private final TimeUnit rateUnit;

    private final double durationFactor;
    private final double rateFactor;

    private final Anodot anodot;
    private final Clock clock;

    private boolean partialStatsMode = false;

    private AnodotReporter(AnodotMetricRegistry registry,
                           Anodot anodot,
                           Clock clock,
                           TimeUnit rateUnit,
                           TimeUnit durationUnit,
                           AnodotMetricFilter filter) {
        this.registry = registry;
        this.executor = Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory("graphite-reporter"));
        this.filter = filter;

        this.durationFactor = 1.0 / durationUnit.toNanos(1);
        this.durationUnit = durationUnit;
        this.rateFactor = rateUnit.toSeconds(1);
        this.rateUnit = rateUnit;

        this.anodot = anodot;
        this.clock = clock;
    }

    public void setPartialStatsMode(boolean partialStatsMode) {
        this.partialStatsMode = partialStatsMode;
        LOGGER.info("Anodot reporter partial stats mode is set to: {}", this.partialStatsMode);
    }

    /**
     * Report the current values of all metrics in the registry.
     */
    public void report() {
        report(registry.getGauges(filter),
                registry.getCounters(filter),
                registry.getHistograms(filter),
                registry.getMeters(filter),
                registry.getTimers(filter));

    }

    public void reportManagedMetrics() {
        try {
            final long timestamp = clock.getTime() / 1000;

            SortedMap<MetricName, ManagedMetric> managedTimers = registry.getManagedTimers(filter);
            Property durationUnit = new Property(Properties.UNIT, extractUnit(this.durationUnit));
            sendManagedMetrics(timestamp, managedTimers, durationUnit);

            SortedMap<MetricName, ManagedMetric> managedHistograms = registry.getManagedHistograms(filter);
            sendManagedMetrics(timestamp, managedHistograms, null);

        } catch (IOException e) {
            LOGGER.warn("Unable to report to Anodot", anodot, e);
            try {
                anodot.close();
            } catch (IOException e1) {
                LOGGER.warn("Error closing Anodot", anodot, e);
            }
        }

    }

    private void sendManagedMetrics(long timestamp,
                                    SortedMap<MetricName, ManagedMetric> managedMetrics,
                                    Property durationUnit) throws IOException {
        for (Map.Entry<MetricName, ManagedMetric> managedMetric : managedMetrics.entrySet()) {

            MetricName metricName = managedMetric.getKey();
            List<ReportInfo> percentilesReportInfos = managedMetric.getValue().getPercentiles();
            sendManagedPercentiles(timestamp, durationUnit, metricName, percentilesReportInfos);

            anodot.send(buildName(metricName, COUNTER_TARGET_TYPE), format(((Counting) managedMetric.getValue()).getCount()), timestamp, TargetType.GAUGE.getName());

            List<ReportInfo> meteredReportInfos = managedMetric.getValue().getMetered();
            sendManagedMetered(timestamp, metricName, meteredReportInfos);
        }
    }

    private void sendManagedMetered(long timestamp, MetricName metricName, List<ReportInfo> meteredReportInfos) throws IOException {
        if (!meteredReportInfos.isEmpty()) {
            Property baseUnit = metricName.getProperty(Properties.UNIT);
            Property unit;
            if (baseUnit == null || baseUnit.getValue().equals(Unit.UNKNOWN.getSymbol())) {
                unit = new Property(Properties.UNIT, "hits_per_" + extractUnit(rateUnit));
            } else {
                unit = new Property(Properties.UNIT, baseUnit.getValue() + "_per_" + extractUnit(rateUnit));
            }

            for (ReportInfo reportInfo : meteredReportInfos) {
                anodot.send(buildName(metricName, unit, RATE_TARGET_TYPE, reportInfo.getProperty()),
                        format(convertRate((double)reportInfo.getValue())),
                        timestamp,
                        reportInfo.getName());
            }
        }
    }

    private void sendManagedPercentiles(long timestamp, Property durationUnit, MetricName metricName, List<ReportInfo> percentilesReportInfos) throws IOException {
        for (ReportInfo reportInfo : percentilesReportInfos) {
            if (durationUnit != null) {
                anodot.send(
                        buildName(metricName, GAUGE_TARGET_TYPE, durationUnit, reportInfo.getProperty()),
                        format(convertDuration(reportInfo.getValue())),
                        timestamp,
                        reportInfo.getName());
            }
            else {
                anodot.send(
                        buildName(metricName, GAUGE_TARGET_TYPE, reportInfo.getProperty()),
                        format(reportInfo.getValue()),
                        timestamp,
                        reportInfo.getName());
            }
        }
    }

    public void report(SortedMap<MetricName, Gauge> gauges,
                       SortedMap<MetricName, Counter> counters,
                       SortedMap<MetricName, Histogram> histograms,
                       SortedMap<MetricName, Meter> meters,
                       SortedMap<MetricName, Timer> timers) {
        final long timestamp = clock.getTime() / 1000;

        // oh it'd be lovely to use Java 7 here
        try {
            for (Map.Entry<MetricName, Gauge> entry : gauges.entrySet()) {
                reportGauge(entry.getKey(), entry.getValue(), timestamp);
            }

            for (Map.Entry<MetricName, Counter> entry : counters.entrySet()) {
                reportCounter(entry.getKey(), entry.getValue(), timestamp);
            }

            for (Map.Entry<MetricName, Histogram> entry : histograms.entrySet()) {
                reportHistogram(entry.getKey(), entry.getValue(), timestamp);
            }

            for (Map.Entry<MetricName, Meter> entry : meters.entrySet()) {
                reportMetered(entry.getKey(), entry.getValue(), timestamp);
            }

            for (Map.Entry<MetricName, Timer> entry : timers.entrySet()) {
                reportTimer(entry.getKey(), entry.getValue(), timestamp);
            }

            reportManagedMetrics();

            anodot.flush();
        } catch (IOException e) {
            LOGGER.warn("Unable to report to Anodot", anodot, e);
            try {
                anodot.close();
            } catch (IOException e1) {
                LOGGER.warn("Error closing Anodot", anodot, e);
            }
        }
    }

    public void stop() {
        try {
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException ignored) {
            // do nothing
        } finally {
            try {
                anodot.close();
            } catch (IOException e) {
                LOGGER.debug("Error disconnecting from Anodot", anodot, e);
            }
        }
    }

    /**
     * Stops the reporter and shuts down its thread of execution.
     */
    public void close() {
        stop();
    }

    /**
     * Starts the reporter polling at the given period.
     *
     * @param period the amount of time between polls
     * @param unit   the unit for {@code period}
     */
    public void start(long period, TimeUnit unit) {
        executor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    report();
                } catch (Exception e) {
                    LOGGER.error("failed to process metrics", e);
                }
            }
        }, period, period, unit);
    }

    private String extractUnit(TimeUnit unit) {
        switch (unit) {
            case NANOSECONDS:
                return Unit.NANO.getSymbol();
            case MICROSECONDS:
                return Unit.MICRO.getSymbol();
            case MILLISECONDS:
                return Unit.MILLI.getSymbol();
            case SECONDS:
                return Unit.SECOND.getSymbol();
            case MINUTES:
                return Unit.MINUTE.getSymbol();
            case HOURS:
                return Unit.HOUR.getSymbol();
            case DAYS:
                return Unit.DAY.getSymbol();
            default:
                throw new IllegalStateException("unknown duration unit: " + durationUnit);
        }
    }

    private void reportTimer(MetricName name, Timer timer, long timestamp) throws IOException {

        Property durationUnit = new Property(Properties.UNIT, extractUnit(this.durationUnit));

        if (partialStatsMode) {
            sendPartialTimerStats(name, timer, timestamp, durationUnit);

        }
        else {
            sendFullTimerStats(name, timer, timestamp, durationUnit);
        }

        reportMetered(name, timer, timestamp);
    }

    private void sendFullTimerStats(MetricName name, Timer timer, long timestamp, Property durationUnit) throws IOException {
        final Snapshot snapshot = timer.getSnapshot();
        anodot.send(
                buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_UPPER),
                format(convertDuration(snapshot.getMax())), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_MEAN),
                format(convertDuration(snapshot.getMean())), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_LOWER),
                format(convertDuration(snapshot.getMin())), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_STD),
                format(convertDuration(snapshot.getStdDev())),
                timestamp,
                TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_P50),
                format(convertDuration(snapshot.getMedian())),
                timestamp,
                TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_P75),
                format(convertDuration(snapshot.get75thPercentile())),
                timestamp,
                TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_P95),
                format(convertDuration(snapshot.get95thPercentile())),
                timestamp,
                TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_P98),
                format(convertDuration(snapshot.get98thPercentile())),
                timestamp,
                TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_P99),
                format(convertDuration(snapshot.get99thPercentile())),
                timestamp,
                TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_P999),
                format(convertDuration(snapshot.get999thPercentile())),
                timestamp, TargetType.GAUGE.getName());
    }

    private void sendPartialTimerStats(MetricName name, Timer timer, long timestamp, Property durationUnit) throws IOException {
        final Snapshot snapshot = timer.getSnapshot();
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_P50),
                format(convertDuration(snapshot.getMedian())),
                timestamp,
                TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_P75),
                format(convertDuration(snapshot.get75thPercentile())),
                timestamp,
                TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_P99),
                format(convertDuration(snapshot.get99thPercentile())),
                timestamp,
                TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, durationUnit, STATS_P999),
                format(convertDuration(snapshot.get999thPercentile())),
                timestamp, TargetType.GAUGE.getName());
    }

    private void reportMetered(MetricName name, Metered meter, long timestamp) throws IOException {
        Property baseUnit = name.getProperty(Properties.UNIT);
        Property unit;
        if (baseUnit == null || baseUnit.getValue().equals(Unit.UNKNOWN.getSymbol())) {
            unit = new Property(Properties.UNIT, "hits_per_" + extractUnit(rateUnit));
        } else {
            unit = new Property(Properties.UNIT, baseUnit.getValue() + "_per_" + extractUnit(rateUnit));
        }

        anodot.send(buildName(name, COUNTER_TARGET_TYPE), format(meter.getCount()), timestamp, TargetType.GAUGE.getName());

        anodot.send(buildName(name, unit, RATE_TARGET_TYPE, INTERVAL_M1),
                format(convertRate(meter.getOneMinuteRate())),
                timestamp,
                TargetType.RATE.getName());
        anodot.send(buildName(name, unit, RATE_TARGET_TYPE, INTERVAL_M5),
                format(convertRate(meter.getFiveMinuteRate())),
                timestamp,
                TargetType.RATE.getName());
        anodot.send(buildName(name, unit, RATE_TARGET_TYPE, INTERVAL_M15),
                format(convertRate(meter.getFifteenMinuteRate())),
                timestamp,
                TargetType.RATE.getName());
        anodot.send(buildName(name, unit, RATE_TARGET_TYPE, STATS_MEAN),
                format(convertRate(meter.getMeanRate())),
                timestamp,
                TargetType.RATE.getName());
    }

    private void reportHistogram(MetricName name, Histogram histogram, long timestamp) throws IOException {
        anodot.send(buildName(name, COUNTER_TARGET_TYPE), format(histogram.getCount()), timestamp, TargetType.GAUGE.getName());

        if (partialStatsMode) {
            sendPartialHistogramStats(name, histogram, timestamp);
        }
        else {
            sendFullHistogramStats(name, histogram, timestamp);
        }
    }

    private void sendFullHistogramStats(MetricName name, Histogram histogram, long timestamp) throws IOException {
        final Snapshot snapshot = histogram.getSnapshot();
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_UPPER), format(snapshot.getMax()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_MEAN), format(snapshot.getMean()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_LOWER), format(snapshot.getMin()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_STD), format(snapshot.getStdDev()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_P50), format(snapshot.getMedian()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_P75), format(snapshot.get75thPercentile()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_P95), format(snapshot.get95thPercentile()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_P98), format(snapshot.get98thPercentile()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_P99), format(snapshot.get99thPercentile()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_P999), format(snapshot.get999thPercentile()), timestamp, TargetType.GAUGE.getName());
    }

    private void sendPartialHistogramStats(MetricName name, Histogram histogram, long timestamp) throws IOException {
        final Snapshot snapshot = histogram.getSnapshot();
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_P50), format(snapshot.getMedian()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_P75), format(snapshot.get75thPercentile()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_P99), format(snapshot.get99thPercentile()), timestamp, TargetType.GAUGE.getName());
        anodot.send(buildName(name, GAUGE_TARGET_TYPE, STATS_P999), format(snapshot.get999thPercentile()), timestamp, TargetType.GAUGE.getName());
    }

    private void reportCounter(MetricName name, Counter counter, long timestamp) throws IOException {
        anodot.send(buildName(name, COUNTER_TARGET_TYPE), format(counter.getCount()), timestamp, TargetType.GAUGE.getName());
    }

    private void reportGauge(MetricName name, Gauge gauge, long timestamp) throws IOException {
        final String value = format(gauge.getValue());
        if (value != null) {
            anodot.send(buildName(name, GAUGE_TARGET_TYPE), value, timestamp, TargetType.GAUGE.getName());
        }
    }

    private String format(Object o) {
        if (o instanceof Float) {
            return format(((Float) o).doubleValue());
        } else if (o instanceof Double) {
            return format(((Double) o).doubleValue());
        } else if (o instanceof Byte) {
            return format(((Byte) o).longValue());
        } else if (o instanceof Short) {
            return format(((Short) o).longValue());
        } else if (o instanceof Integer) {
            return format(((Integer) o).longValue());
        } else if (o instanceof Long) {
            return format(((Long) o).longValue());
        }
        return null;
    }

    private String buildName(MetricName name, Property... properties) {
        MetricNameWrapper wrapper = new MetricNameWrapper(name, properties);
        return wrapper.asString();
    }

    private String format(long n) {
        return Long.toString(n);
    }

    private String format(double v) {
        return String.format(Locale.US, "%2.10f", v);
    }

    protected TimeUnit getRateUnit() {
        return rateUnit;
    }

    protected TimeUnit getDurationUnit() {
        return durationUnit;
    }

    protected double convertDuration(double duration) {
        return duration * durationFactor;
    }

    protected double convertDuration(Number duration) {
        return convertDuration(duration.doubleValue());
    }

    protected double convertRate(double rate) {
        return rate * rateFactor;
    }

    /**
     * Returns a new {@link Builder} for {@link AnodotReporter}.
     *
     * @param registry the registry to report
     * @return a {@link Builder} instance for a {@link AnodotReporter}
     */
    public static Builder forRegistry(AnodotMetricRegistry registry) {
        return new Builder(registry);
    }

    /**
     * A simple named thread factory.
     */
    @SuppressWarnings("NullableProblems")
    private static class NamedThreadFactory implements ThreadFactory {
        private final ThreadGroup group;
        private final AtomicInteger threadNumber = new AtomicInteger(1);
        private final String namePrefix;

        private NamedThreadFactory(String name) {
            final SecurityManager s = System.getSecurityManager();
            this.group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
            this.namePrefix = "metrics-" + name + "-thread-";
        }

        public Thread newThread(Runnable r) {
            final Thread t = new Thread(group, r, namePrefix + threadNumber.getAndIncrement(), 0);
            t.setDaemon(true);
            if (t.getPriority() != Thread.NORM_PRIORITY) {
                t.setPriority(Thread.NORM_PRIORITY);
            }
            return t;
        }
    }

    /**
     * A builder for {@link AnodotReporter} instances. Defaults to not using a enrichName, using the
     * default clock, converting rates to events/second, converting durations to milliseconds, and
     * not filtering metrics.
     */
    public static class Builder {
        private final AnodotMetricRegistry registry;
        private Clock clock;
        private String prefix;
        private TimeUnit rateUnit;
        private TimeUnit durationUnit;
        private AnodotMetricFilter filter;
        private boolean partialStatsMode = false;

        private Builder(AnodotMetricRegistry registry) {
            this.registry = registry;
            this.clock = Clock.defaultClock();
            this.prefix = null;
            this.rateUnit = TimeUnit.SECONDS;
            this.durationUnit = TimeUnit.MILLISECONDS;
            this.filter = AnodotMetricFilter.ALL;
        }

        /**
         * Use the given {@link Clock} instance for the time.
         *
         * @param clock a {@link Clock} instance
         * @return {@code this}
         */
        public Builder withClock(Clock clock) {
            this.clock = clock;
            return this;
        }

        /**
         * Prefix all metric names with the given string.
         *
         * @param prefix the prefix for all metric names
         * @return {@code this}
         */
        public Builder prefixedWith(String prefix) {
            this.prefix = prefix;
            return this;
        }

        /**
         * Convert rates to the given time unit.
         *
         * @param rateUnit a unit of time
         * @return {@code this}
         */
        public Builder convertRatesTo(TimeUnit rateUnit) {
            this.rateUnit = rateUnit;
            return this;
        }

        public Builder setPartialReporting(boolean partial) {
            this.partialStatsMode = partial;
            return this;
        }

        /**
         * Convert durations to the given time unit.
         *
         * @param durationUnit a unit of time
         * @return {@code this}
         */
        public Builder convertDurationsTo(TimeUnit durationUnit) {
            this.durationUnit = durationUnit;
            return this;
        }

        /**
         * Only report metrics which match the given filter.
         *
         * @param filter a {@link MetricFilter}
         * @return {@code this}
         */
        public Builder filter(AnodotMetricFilter filter) {
            this.filter = filter;
            return this;
        }

        /**
         * Builds a {@link AnodotReporter} with the given properties, sending metrics using the
         * given {@link Anodot}.
         *
         * @param anodot a {@link Anodot}
         * @return a {@link AnodotReporter}
         */
        public AnodotReporter build(Anodot anodot) {
            AnodotReporter reporter = new AnodotReporter(
                    registry,
                    anodot,
                    clock,
                    rateUnit,
                    durationUnit,
                    filter);
            reporter.setPartialStatsMode(this.partialStatsMode);
            return reporter;
        }
    }
}



