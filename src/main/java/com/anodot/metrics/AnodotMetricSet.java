package com.anodot.metrics;

import com.anodot.metrics.spec.MetricName;
import com.codahale.metrics.Metric;

import java.util.Map;

/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public interface AnodotMetricSet {
    /**
     * A map of metric names to metrics.
     *
     * @return the metrics
     */
    Map<MetricName, Metric> getMetrics();
}
