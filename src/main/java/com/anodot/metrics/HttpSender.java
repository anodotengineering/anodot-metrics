package com.anodot.metrics;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.SocketConfig;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;


/**
 * User: Yonatan Ben-Simhon
 * Version: Anodot 1.0
 */
public class HttpSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpSender.class);

    public HttpSender() {

//        int timeout = 5; // seconds
//        HttpParams httpParams = httpClient.getParams();
//        httpParams.setParameter(
//                  CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
//        httpParams.setParameter(
//                  CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);
// httpParams.setParameter(
//   ClientPNames.CONN_MANAGER_TIMEOUT, new Long(timeout * 1000));


    }

    private HttpClient setProxy(String proxyHost,Integer proxyPort){


        HttpHost proxy = new HttpHost(proxyHost,proxyPort);
        return HttpClients
                .custom()
                .setProxy(proxy)
                .setDefaultSocketConfig(
                        SocketConfig
                                .custom()
                                .setSoTimeout(20 * 1000)
                                .build()
                ).setDefaultRequestConfig(
                        RequestConfig
                                .custom()
                                .setConnectTimeout(20 * 1000)
                                .build())
                .build();
    }

    private HttpClient getClient() {
        String proxyHost=System.getProperty("metrics_proxy_host");
        String proxyPortStr=System.getProperty("metrics_proxy_port");
        int proxyPort=0;
        try {
            proxyPort = Integer.parseInt(proxyPortStr);
        }catch(NumberFormatException e){
            proxyPort=0;
        }
        if(proxyHost==null || proxyPort==0) {
            return HttpClients
                    .custom()
                    .setDefaultSocketConfig(
                            SocketConfig
                                    .custom()
                                    .setSoTimeout(20 * 1000)
                                    .build()
                    ).setDefaultRequestConfig(
                            RequestConfig
                                    .custom()
                                    .setConnectTimeout(20 * 1000)
                                    .build())
                    .build();
        }else {
            return  setProxy(proxyHost,proxyPort);
        }

    }

    public void post(URI uri, String data) {
        HttpPost httpPost = new HttpPost(uri);
        httpPost.setEntity(new StringEntity(data, ContentType.APPLICATION_JSON));

        try {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace(httpPost.toString());
            }

            HttpClient client = getClient();
            HttpResponse response = client.execute(httpPost);

            StatusLine statusLine = response.getStatusLine();
            if (statusLine != null) {
                int statusCode = statusLine.getStatusCode();
                if (statusCode != 200) {
                    LOGGER.error("failed sending to Anodot with status code: {}, line: {}", statusCode, statusLine.getReasonPhrase());
                }
            }

            cleanClose(response);
        } catch (IOException e) {
            LOGGER.error("error posting to: " + uri, e);
        }
    }

    private void cleanClose(HttpResponse response) throws IOException {
        if (response != null) {
            try {
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace(response.getStatusLine().toString());
                }

                //  ensure response is fully consumed
                EntityUtils.consumeQuietly(response.getEntity());
            } finally {
                if (response instanceof Closeable)
                    ((Closeable) response).close();
            }
        }
    }
}