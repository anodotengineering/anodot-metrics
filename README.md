# README #

### What is this repository for? ###

* Anodot metrics is an extension of the widely used [Coda-Hale Metrics](https://github.com/dropwizard/metrics) library for measuring metrics of Java based applications.
* This short setup instruction page assumes you have some experience with [Coda-Hale Metrics](https://github.com/dropwizard/metrics), if that's not the case, first follow the [getting started](https://dropwizard.github.io/metrics/3.1.0/getting-started/) page of Coda-Hale Metrics.
* Version - 3.0.2.


### Setting up using Maven ###
The jar is available in our [public Maven repository](https://bitbucket.org/anodotengineering/anodot-maven-repo-release/raw/master) hosted by bitbucket.

* Add the repository to your pom xml:
```
#!xml

<repositories>
        <repository>
            <id>anodot-maven-repo-release</id>
            <name>anodot-maven-repo-release</name>
            <releases>
                <enabled>true</enabled>
            </releases>
            <url>https://bitbucket.org/anodotengineering/anodot-maven-repo-release/raw/master</url>
        </repository>
</repositories>
```


* In order to connect to the bitbucket external repository you will need the wagon git plugin. 
Add it to your pom plugin repository by adding the following to your pom XML:
```
#!xml

<pluginRepositories>
        <pluginRepository>
            <id>synergian-repo</id>
            <url>https://raw.github.com/synergian/wagon-git/releases</url>
        </pluginRepository>
</pluginRepositories>
```
* In order to use wagon-git add the extension to your build extensions:
```
#!xml

<build>
        <extensions>
            <extension>
                <groupId>ar.com.synergian</groupId>
                <artifactId>wagon-git</artifactId>
                <version>0.2.5</version>
            </extension>
        </extensions>
</build>
```
* Lastly, add the anodot-metrics jar dependency to you pom XML:

```
#!xml

<dependencies>
      <dependency>
            <groupId>io.anodot</groupId>
            <artifactId>anodot-metrics</artifactId>
            <version>3.0.2</version>
      </dependency>
</dependencies>
```

### Dependencies ###
anodot-metrics depend on the following libraries:

1. metrics-core version 3.0.2
2. slf4j-api version 1.7.5
3. apache httpclient version 4.5

### Start sending metrics ###
* First you need to setup an instance of a AnodotMetricRegistry, this is the container of all your metrics:

```
#!java

metricRegistry = new AnodotMetricRegistry();
```
* Anodot uses a structured key-value based metric names. 
In order to create a metric you must first define a MetricName object and assign it some properties (metric uniqueness in Anodot is determined by the set of unique property keys and values constructing the name). 

For example a typical name might look something like:

```
#!java

MetricName name = 
      MetricName.builder("response_time_of_my_method")
                .withPropertyValue("server", "vm10")
                .withPropertyValue("data_center", "aus")
                .withPropertyValue("component", "rest_api")
                .build();

```
First argument of the builder method is the "what" property which is a must property that represents what you are actually measuring. 

You may add additional properties using the "withPropertyValue" method call expecting two arguments for the property key and value respectively.
The example above will report a metric that will appear as "server=vm10.data_center=aus.component=rest_api.what=response_time_of_my_method" in Anodot system.

**Note:** Anodot recommends the metric naming convention suggested by [Metrics 2.0](http://metrics20.org/). 

There are many constants that follow the 2.0 spec, for the most commonly used properties take a look at [com.anodot.metrics.spec.Properties](https://bitbucket.org/anodotengineering/anodot-metrics/src/3f4ed4c973c7809492962a4d1639e5de353dc6c0/src/main/java/com/anodot/metrics/spec/Properties.java?at=master&fileviewer=file-view-default). 

For some unit examples take a look at: [com.anodot.metrics.spec.Unit](https://bitbucket.org/anodotengineering/anodot-metrics/src/3f4ed4c973c7809492962a4d1639e5de353dc6c0/src/main/java/com/anodot/metrics/spec/Unit.java?at=master&fileviewer=file-view-default).

Now that we have constructed a valid metric name we can create a metric using the AnodotMetricRegistry API. For example, in order to create a Timer one would do the following:

```
#!java

Timer timer = metricRegistry.timer(name);
```

For information of how to create other types of metrics and how to embed them in your code follow the [getting started](https://dropwizard.github.io/metrics/3.1.0/getting-started/) page of [Coda-Hale Metrics](https://github.com/dropwizard/metrics).
 
* Next you would like to connect your AnodotMetricRegistry to an AnodotReporter. A reporter is what periodically sends your registry metrics in to Anodot system. For example, in order to report your metrics periodically every 5 minutes do the following:

```
#!java

AnodotReporter
           .forRegistry(metricRegistry)               
           .convertDurationsTo(TimeUnit.MILLISECONDS) // measure timers in millisecond
           .convertRatesTo(TimeUnit.SECONDS)          // report rates per second
           .build(new Anodot("https://api.anodot.com/api/v1/metrics", "API_TOKEN"))
           .start(300, TimeUnit.SECONDS);
```
**Note:**

1. Replace API_TOKEN with your real API token of your Anodot account.
2. AnodotReporter may add some additional properties into the metric names based on the type of metrics you define. For example "target_type" represents the type of the metric and may be one of the following: "gauge" "timer" "rate".

### Need help? ###
* Contact our support at support@anodot.com
